<?php

//This class represents a custom data structure for our items, and also a helper class for the items.
//The helper functionality will help us get data for each db table.
class Item 
{
    private $which_sheet;       //string
    private $item_name;         //string
    private $item_desc;         //string
    private $brand_name;        //string
    private $item_qty;          //number
    private $purchase_date;     //datetime
    private $est_restock_date;  //datetime
    private $sale_date;         //datetime
    private $sale_qty;          //number

    public function __construct($which_sheet, $item_name, $item_desc, $brand_name, $item_qty, $purchase_date, $est_restock_date, $sale_date, $sale_qty) 
    {
        //Assign all vars.
        $this->which_sheet = $which_sheet;
        $this->item_name = $item_name;
        $this->item_desc = $item_desc;
        $this->brand_name = $brand_name;
        $this->item_qty = $item_qty;
        $this->purchase_date = $purchase_date;
        $this->est_restock_date = $est_restock_date;
        $this->sale_date = $sale_date;
        $this->sale_qty = $sale_qty;
    }

    public function GetItem()
    {
        return array(
            "which_sheet" => $this->which_sheet,
            "item_name" => $this->item_name,
            "item_desc" => $this->item_desc,
            "brand_name" => $this->brand_name,
            "item_qty" => $this->item_qty
        );
    }

    public function GetPurchase()
    {
        return array(
            "purchase_date" => $this->purchase_date,
            "est_restock_date" => $this->est_restock_date
        );
    }

    public function GetSale()
    {
        return array(
            "sale_date" => $this->sale_date,
            "sale_qty" => $this->sale_qty
        );
    }
}

?>