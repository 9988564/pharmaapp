function WriteAjaxData(theUrl, theElement)
{
    var returnVal = "";
    $.ajax(
    {
        url: theUrl,
        type: "post", // To protect sensitive data
        data: 
        {
            ajax:true,
        },
        success: function(response)
        {
            $(theElement).empty();
            $(theElement).append("<br><br>" + response);
        }
    })
}

//Create an event handler for the DOCUMENT because the form is dynamically generated. That way this will be called when needed.
//This creates an event handler for button click of view-sales-button
$(document).on('click', '.view-sales-button', function(event)
{
    var spreadsheet_id = $('#'+this.id).data('spreadsheet_id');
    var item_id = $('#'+this.id).data('item_id');
    
    $.ajax(
    {
        url: "ControlPanel/ViewSales",
        type: "post", // To protect sensitive data
        data: 
        {
            spreadsheet_id: spreadsheet_id,
            item_id: item_id
        },
        success: function(response)
        {
            $data = JSON.parse(response);
            $table = $data[0];
            $array = $data[1];
            if ($('#spreadsheet-sales').length)
            {
                $('#spreadsheet-sales').remove();
            }

           
            $('#the-spreadsheet').append("<div id='spreadsheet-sales'><h2>Sales of Selected Item</h2>" + $table + "</div>");

             $arrayForBurnDownBuffer = [[],[]];
           for(var i =0; i < $array.length ; i++)
           {
           console.log($array[i]["Date of Sale"]);
           console.log($array[i]["Quantity of Sale"]);
           $arrayForBurnDownBuffer[0][i]= $array[i]["Date of Sale"];
           $arrayForBurnDownBuffer[1][i] = $array[i]["Quantity of Sale"];
          }
          var burnDownChart;
          console.log($arrayForBurnDownBuffer);
          initialiseBurnDownChart('burn-down-chart',burnDownChart);
          executeUpdateChartsWithRegression('burn-down-chart',id,$arrayForBurnDownBuffer);
        }
    })


    //Prevent the default action from taking place (fallback to the submission anyway - just in case.)
    event.preventDefault();
});

//Create an event handler for the DOCUMENT because the form is dynamically generated. That way this will be called when needed.
//This submits the new spreadsheet to the database.
$(document).on('submit', '#add-spreadsheet-form', function(event)
{
    $.ajax(
    {
        url: "ControlPanel/CreateSpreadSheet",
        type: "post", // To protect sensitive data
        data: $(this).serialize(),
        success: function(response)
        {
            $('#add-spreadsheet-form').append("<br><br>" + response);
            $('#add-spreadsheet-form')[0].reset();
        }
    })


    //Prevent the default action from taking place (fallback to the submission anyway - just in case.)
    event.preventDefault();
});

//Create an event handler for the DOCUMENT because the form is dynamically generated. That way this will be called when needed.
//This inserts the new item to the existing spreadsheet on the database.
$(document).on('submit', '#insert-item-form', function(event)
{
    $.ajax(
    {
        url: "ControlPanel/InsertItemToSpreadSheet",
        type: "post", // To protect sensitive data
        data: $(this).serialize(),
        success: function(response)
        {
            $('#insert-item-form').append("<br><br>" + response);
            $('#insert-item-form')[0].reset();
        }
    })


    //Prevent the default action from taking place (fallback to the submission anyway - just in case.)
    event.preventDefault();
});

//Create an event handler for the DOCUMENT because the form is dynamically generated. That way this will be called when needed.
//This handles clicks for a spreadsheet (to expand and view it)
$(document).on('click', '.spreadsheet-view-button', function(event)
{
    if ($('#spreadsheet-sales').length)
    {
        $('#spreadsheet-sales').remove();
    }


    $('#the-spreadsheet').html('');
    id = this.id.split("-")[1];
    // console.log(id);
    
                // WriteAjaxData("ControlPanel/GetSpreadsheets", "#the-spreadsheet");
                $.ajax(
                {
                    url: "ControlPanel/ViewSpreadSheet",
                    type: "post", // To protect sensitive data
                    data:
                    {
                        spreadsheet_id: id
                    },
                    success: function(response)
                    {
               
                            // $.getScript(window.location.origin + '/pharmaapp/assets/js/dynamicview.js');
                            // $.getScript(window.location.origin + '/pharmaapp/assets/js/BurnDownChartHandler.js');
                            // $.getScript(window.location.origin + '/pharmaapp/assets/js/highcharts.js');
                            // $.getScript(window.location.origin + '/pharmaapp/assets/js/CSVHandler.js');
                            
                        /***********************************/
                        $('#the-spreadsheet').append("<h2>Overview of Selected Spreadsheet</h2>" + response);
                        // $('#the-spreadsheet').append("<br><br>" + response);
                        $('#the-spreadsheet').append("<br><br><p id = \'CSVFileLocationInput\'></p> <br><br>")
                        $('#the-spreadsheet').append("<p id = \'burn-down-chart\'></p>");
                        console.log(response);
                        var burnDownChart;
            
                        populateCSVFileLocationInput();
                            $('#ImportCSVFileButton').click(function(){	
                                readCSVFile();
                            });	
                         $.ajax({
                                 url: "ControlPanel/populateBurnDownChart",
                                 type: "post", // To protect sensitive data
                                 data:
                                 {
                                     Bspreadsheet_id: id
                                 },
                                 success: function(response)
                                 {
                                     $arrayForBurnDownRaw = JSON.parse(response);
                                    //  $arrayForBurnDownBuffer = [[],[]];
                                    //  for(var i =0; i < $arrayForBurnDownRaw.length ; i++)
                                    //  {
                                    //  console.log($arrayForBurnDownRaw[i]["Date of Sale"]);
                                    //  console.log($arrayForBurnDownRaw[i]["Quantity of Stock"]);
                                    //  $arrayForBurnDownBuffer[0][i]= $arrayForBurnDownRaw[i]["Date of Sale"];
                                    //  $arrayForBurnDownBuffer[1][i] = $arrayForBurnDownRaw[i]["Quantity of Stock"];
                                    // }
                                    
                                    // console.log($arrayForBurnDownBuffer);
                                    // initialiseBurnDownChart('burn-down-chart',burnDownChart);
                                    // executeUpdateChartsWithRegression('burn-down-chart',id,$arrayForBurnDownBuffer);	//remove after testing the burn down algorithm  
                                 }
                         })
                           
                        
                    }
                })

    //Prevent the default action from taking place (fallback to the submission anyway - just in case.)
    event.preventDefault();
});

$( document ).ready(function() 
{
    // $( "#sheet_submit" ).click(function() {
    //     $( "#add-spreadsheet-form" ).submit();
    // });


    //Create an event for the select box
    $( ".main-selector" ).change(function() 
    {
        //get selected
        var selected = $('.main-selector').find(":selected").attr('value');
        //switch between options:

        //Hide all
        $('#view-existing-container').hide();
        $('#create-new-container').hide();
        $('#add-to-existing-container').hide();

        //Switch and show the correct container.
        switch(selected)
        {
            case "view":
            {
                WriteAjaxData("ControlPanel/GetSpreadsheets", "#view-existing");
                $('#view-existing-container').show();
                break;
            }
            case "new":
            {
                WriteAjaxData("ControlPanel/ShowAddSpreadsheetForm", "#create-new");
                $('#create-new-container').show();
                break;
            }
            case "add":
            {
                WriteAjaxData("ControlPanel/ShowAddItemToExistingForm", "#add-to-existing");
                $('#add-to-existing-container').show();
                break;
            }
        }
    });
});