//purpose of this javascript file is to interface the view to the datacontroller
//updating the controller on any changes in the view

$(document).ready(function()
{
	
    /**
     * data model ajax functions
     */
	$.getScript('../assets/js/CSVHandler.js');
	$.getScript('../assets/js/BurnDownChartHandler.js');
	$.getScript('../assets/js/highcharts.js');
	// testCSVHandler();
	//to get functions to dynamically populate html tags
	$.getScript('../assets/js/dynamicview.js',function(){
	
			/* initialisation functions */
				populateItemListForChart();
				//initialise the item burn down chart 
				var burnDownChart;
				initialiseBurnDownChart(burnDownChart);



			//Clicked View All Uploaded Documents
			$("#ViewAllUploadedDocumentsButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/GetUploadedDocuments",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});


			//Clicked View All Created Diagrams
			$("#ViewAllCreatedDiagramsButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/GetCreatedDiagrams",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});

			//Clicked Upload Document
			//this option will give you the option to upload a csv file
			$("#UploadDocumentButton").click(function()
			{
				//populate a input field to input file location of the csv file
				populateCSVFileLocationInput();
				// $('#ImportCSVFileButton').click(function(){	
					readCSVFile();
			    //  var CSVFormData = new FormData();
				//  CSVFormData.append("CSVFileLocation",$("CSVFileLocation").val());
				//  var processCSV = function() {
				// 	 console.log("CSVFormData made!");
				// 	 $.ajax(
				// 		    {
				// 		    	url: "datacontroller/ProcessCSVFile",
				// 		    	type: "post", // To protect sensitive data
				// 	            data : CSVFormData,
				// 				async: false,
				// 		    	success: function(response)
				// 		    	{
				// 					console.log("UploadDocumentButton ajax working")
				// 		    		console.log(response);
				// 		    	},
				// 				cache: false,
				// 				contentType: false,
				// 				processData: false
				// 		})
				// 		return false;
				//  }
				// });	
			});

			


			//Clicked Create Diagram
			$("#CreateDiagramButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/AddDiagram",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});



			//Clicked Download All Diagrams
			$("#DownloadAllDiagramsButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/DownloadAllDiagrams",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});


			//Clicked Download All Documents
			$("#DownloadAllDocumentsButton").click(function()
			{
				exportCSVFileLocationInput();
				$('#ExportCSVFileButton').click(function(){	
					var ItemID = document.getElementById("exportCSVItemID").value;
					var ItemName = document.getElementById("exportCSVItemName").value;
					var fileLocation = document.getElementById("exportCSVFileLocation").value;
					
					$data = [ItemID,ItemName,fileLocation];
					$.ajax(
					{
						url: "datacontroller/ExportCSVFile",
						type: "post", // To protect sensitive data
						data: 
						{
							ajax:true,
							'CSVExportInformation':$data
						},
						success: function(response)
						{
							console.log(response);
						}
					})
				});
			});



			//Clicked Delete All Diagrams
			$("#DeleteAllDiagramsButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/DeleteAllDiagrams",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});



			//Clicked Delete All Documents
			$("#DeleteAllDocumentsButton").click(function()
			{
				$.ajax(
				{
					url: "datacontroller/DeleteAllDocuments",
					type: "post", // To protect sensitive data
					data: 
					{
						ajax:true,
					},
					success: function(response)
					{
						console.log(response);
					}
				})
			});

			//items have been Clicked, to show the CSV burn down chart for the item clicked
			$('#itemsList').on("click", ".items", function() {
			var itemID = this.id;
			console.log(itemID);
			var dataList = [];
					$.ajax(
						{
							url: "datacontroller/UpdateBurnDownChart",
							type: "get", 
							data: {ajax: true, 'itemID': itemID},
							
							success: function(response)
							{
								console.log("item selected!");
								console.log(response);
								var dataBuffer = JSON.parse(response);
								
								
								for(var i=0;i<dataBuffer.length;i++)
								{
									
									dataList.push(Number(dataBuffer[i].qty));
								}
								updateChart(dataList ,itemID);
								testUpdateChartsWithRegression();	//remove after testing the burn down algorithm

							},
							error: function(XMLHttpRequest, textStatus, errorThrown){
								console.log("Status: " + textStatus);
								console.log("Error: " + errorThrown);
							}
						})   
			return false;
			});



			//Dynamically update user inputs for adding an item
			$("#AddItemButton").click(function(){
					console.log("AddItemButton pressed!!");
					populateAddItemFields();
			});

			$("#EditItemButton").click(function(){
					console.log("AddItemButton pressed!!");
					populateEditItemFields();
			});

			$("#DeleteItemButton").click(function(){
					console.log("AddItemButton pressed!!");
					populateDeleteItemFields();
			});

			$("#SellItemButton").click(function(){
					console.log("sellItemButton pressed!!");
					populateSellItemFields();
			});
			

			// function populateBurnDownChart()
			// {
			// 	updateChart(dataArray, itemID);
				
			// 	setInterval(function() {
			// 		populateBurnDownChart();
			// 	}, 5000); //5 seconds
			// }populateBurnDownChart();

		

			// function testpopulateItemList()
			// {
			// 	$inner1  = ['ID1','Name1','Brand1','Description1','Qty1']
			// 	$inner2  = ['ID2','Name2','Brand2','Description2','Qty2']
			// 	$inner3  = ['ID3','Name3','Brand3','Description3','Qty3']
			// 	$data = [];
			// 	$data[0] = $inner1;
			// 	$data[1] = $inner2;
			// 	$data[2] = $inner3;
				
			// 	populateItemList($data)
			// }testpopulateItemList();


	});

});