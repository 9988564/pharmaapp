
function updateChart($appendToTagID, dataArray, itemID) {
		
			burnDownChart = new Highcharts.chart($appendToTagID, {
								
								series: [{
									 name: itemID,
									 data: dataArray
								}]
							});	
	}	

function initialiseBurnDownChart($appendToTagID,$burnDownVar)
    {
        $burnDownVar = new Highcharts.chart($appendToTagID, {
								chart: {
									type: 'bar'
								},
								title: {
									text: 'Item Burn Down Chart'
								},
								xAxis: {
									categories: ['date 1', 'date 2', 'date 3']
								},
								yAxis: {
									title: {
										text: 'Please Select an Item'
									}
								},
								
								series: [{
									 data: []
								}]
							});
    }


    //update the burn down chart 
	

	function testUpdateChartsWithRegression($appendToTagID)
	{
		console.log("test update chart ")
		$itemID = "102";
		$arrayOfData = [["01/04/2017"
						,"02/04/2017"    
 						,"03/04/2017"    
 						,"04/04/2017"    
 						,"05/04/2017"    
 						,"06/04/2017"    
 						,"07/04/2017"    
 						,"08/04/2017"    
 						,"09/04/2017"    
 						,"10/04/2017"    
 						,"11/04/2017"    
 						,"12/04/2017"    
 						,"13/04/2017"    
 						,"14/04/2017"    
 						,"15/04/2017"    
 						,"16/04/2017"    
 						,"17/04/2017"    
 						,"18/04/2017"    
 						,"19/04/2017"    
 						,"20/04/2017"    
 						,"21/04/2017"    
 						,"22/04/2017"    
 						,"23/04/2017"    
 						,"24/04/2017"    
 						,"25/04/2017"    
 						,"26/04/2017"    
 						,"27/04/2017"],
						 [700
						 ,450
						 ,450
						 ,450
						 ,450
						 ,400
						 ,500
						 ,300
						 ,300
						 ,300
						 ,300
						 ,300
						 ,300
						 ,300
						 ,200
						 ,200
						 ,400
						 ,200
						 ,200
						 ,200
						 ,700
						 ,200
						 ,200
						 ,200
						 ,200
						 ,200
						 ,100]];
		$windowAverageSize = 8;
		$windowRegressionSize = 20;
	    updateChartsWithRegression($appendToTagID,$itemID,$arrayOfData,$windowAverageSize,$windowRegressionSize);
	}

	function executeUpdateChartsWithRegression($appendToTagID,$sheetID,$dataArray)
	{
		console.log("executeUpdateChartsWithRegression")
		console.log("$appendToTagID" + $appendToTagID);
		console.log("$shetID" + $sheetID);
		console.log("$dataArray" + $dataArray);
		$itemID = "102";
		$windowAverageSize = 3;
		$windowRegressionSize = 3;
	    updateChartsWithRegression($appendToTagID,$itemID,$dataArray,$windowAverageSize,$windowRegressionSize);
	}


	function processDataForBurnDownChart($appendToTagID,$data)
	{
		//$data[0] = $itemID
		//$data[1] = $itemID
		//$data[2] = $itemID
		//$data[3] = $itemID
		//$data[4] = $itemID
		//$data[5] = $itemID
		$windowAverageSize = 8;
		$windowRegressionSize = 20;

	}

	function updateChartsWithRegression($appendToTagID,$itemID, $arrayOfData, $windowAverageSize, $windowRegressionSize)
	{
		//$arrayOfData[0] = $dataArrayDates[]
		//$arrayOfData[1] = $dataArray[]
		console.log("$arrayOfData: \n"+$arrayOfData[1]);
		$regressionLineData = burnDownEstimationAlgorithm($itemID,$arrayOfData[1].map(Number),$arrayOfData[0],$windowAverageSize,$windowRegressionSize);
		console.log("$regressionLineData: \n"+$regressionLineData);
		$regressionDataPoints = regressionLineDataPoints($regressionLineData,$arrayOfData[0]);
		console.log("$regressionDataPoints: \n"+$regressionDataPoints);
		$averagedData = windowAverage($windowAverageSize,$arrayOfData[1].map(Number),$arrayOfData[0]);
		console.log("$averagedData: \n"+$averagedData);
		burnDownChart = new Highcharts.chart($appendToTagID,{
								title: {
									text: 'Item Burn Down Chart'
								},
								xAxis: {
											categories: $arrayOfData[0]
									   },
								series: [
									{
										name: 'itemData',
										data: $arrayOfData[1].map(Number)
									}
									,{
										name: "Projected Item Sales",
										data: $regressionDataPoints        
									},
									{
										name: "window averaged data",
										data: $averagedData[1]
									}
								]
		});
	}


	function burnDownEstimationAlgorithm($itemName,$itemData,$itemDataDates,$windowAverageSize,$windowRegressionSize)
	{
		if($itemData.length == $itemDataDates.length)
		{
			//$itemData rows: item qty Note: algorithm will only do 1 row for now to make it simple
			//once one row is working, adjust the algorithm to do multiple rows
			//$itemData columns: date
			

			//window average for item qty
			$averagedData = windowAverage($windowAverageSize,$itemData,$itemDataDates);
			$regressionLineData = regressionGradientCalc($windowRegressionSize,$itemDataDates,$itemData);
			return $regressionLineData;
		}
		else
		{
			console.log("burnDownEstimationAlgorithm: $itemData.length is not the same as $itemDataDates.length!");
		}
	}

	//this function will average out the data points
	function windowAverage($windowSize,$data,$dataDates)
	{
		$averagedData = [];
		for(var i = 0; i<Number($windowSize)-1;i++)	//move averaged data forward be window size 
		{
			$averagedData.push("null");
		}
		var $averagedBuffer = 0;	//store the averaged data for calculation and appending
		
		for(var i = 0; i < $data.length; i++)
		{
			for(var j = 0; j < Number($windowSize); j++)	//average out the datapoints with window size
			{
				if($data.length > (i + Number($windowSize)))	//check if the window is within the data length
				{
					// console.log("windowAverage: " + $data[i + j]);
					$averagedBuffer += Number($data[i + j]);
					// console.log("windowAverage: $averagedBuffer: " + $averagedBuffer);
				}
				else{
					$averagedBuffer += $data[$data.length-1];
				}
				
			}
			// console.log("windowAverage: $averagedBuffer: after loop:" + $averagedBuffer);
			//obtain average data point 
			$averaged = $averagedBuffer/(Number($windowSize));
			// console.log("windowAverage: $averaged: after loop:" + $averaged);
			$averagedData.push($averaged);
			// console.log("$averagedData: " + $averagedData);
			$averagedBuffer = 0;
		}


		$return = [$dataDates,$averagedData];
		return $return;
	}


	function convertStringDateArrayIntoMillisecondArray($data)
	{
		       $date = [];
			   var temp = $data;
			   console.log(temp);
				for(var i = 0;i < temp.length; i++)
				{
					var dateTimeTemp = temp[i].split(" ");
					$dateBuffer = dateTimeTemp[0].split("-");
					$timeBuffer = dateTimeTemp[1].split(":");
					$seconds = $timeBuffer[2];
					$minutes = $timeBuffer[1];
					$hours = $timeBuffer[0];
					$day = $dateBuffer[2];
					$month = $dateBuffer[1];
					$year = $dateBuffer[0];
					// Date(year, month, day, hours, minutes, seconds, milliseconds);
					var d = new Date($year,$month,$day,$hours,$minutes,$seconds);
					// var d = new Date($data);
					$date.push(d);
				}
				console.log("$date:" + $date);
				//convert date format into milliseconds for calculation
				$date = $date.map(Date.parse);
				console.log("$date: parsed: " + $date);
				return $date;
	}
	//this function will determine the gradient of the trend and the mid point where to place the line to be drawn
	function regressionGradientCalc($windowSize,$xAxis,$yAxis)
	{
		console.log("xAxis: " + $xAxis);
		

	
		console.log("yAxis: " + $yAxis);
		console.log("windowSize: " + $windowSize)
		if($xAxis.length == $yAxis.length)
		{
			
			if($windowSize < $xAxis.length && $windowSize < $yAxis.length)
			{
				//convert to string to date 
				$date = convertStringDateArrayIntoMillisecondArray($xAxis);
				//obtain the middle of the dataset 
				//obtain middle of xaxis
				$xBuffer = 0;
				for(var i = 0; i <$date.length; i++)
				{
					$xBuffer += $date[i];
				}
				$midXAxis = ($xBuffer)/$date.length;
				$midXData = Math.round(($date.length)/2);
				//obtain middle of yaxis
				$yBuffer = 0;
				for(var i = 0; i <$yAxis.length; i++)
				{
					$yBuffer += $yAxis[i];
				}
				$midYAxis = ($yBuffer)/$yAxis.length;
				$midYData = Math.round(($yAxis.length)/2);
				console.log("$midYAxis: " + $midYAxis);
				console.log("$midYData: " + $midYData);
				console.log("$midXAxis: " + $midXAxis);
				console.log("$midXData: " + $midXData);
				
				//get start point of window from mid point
				$startXAxis = $date[$midXData - Math.round($windowSize/2)];
				$startYAxis = $yAxis[$midYData - Math.round($windowSize/2)];
				console.log("$startXAxis: " + $startXAxis);
				console.log("$startYAxis: " + $startYAxis);
				//get end point of window from mid point
				$endXAxis = $date[$midXData + Math.round($windowSize/2)];
				$endYAxis = $yAxis[$midYData + Math.round($windowSize/2)];
				console.log("$endXAxis: " + $endXAxis);
				console.log("$endYAxis: " + $endYAxis);
				//calculate gradient
				$gradient = ($endYAxis - $startYAxis)/($endXAxis - $startXAxis);
				console.log("$gradient: " + $gradient);
				//store values in array to be returned
				return $result = [$midXAxis,$midYAxis,$gradient];
			}
			else{
				console.log("regressionGradientCalc: window size is larger than dataset!!");
			}
		}
		else{
			console.log("regressionGradientCalc: xAxis.length is not the same as yAxis.length");
		}


		// 	//convert back to date
		// $epochTime = [];
		// for(var i = 0; i< $date.length;i++)
		// {
		// 	var d = new Date(0);
		// 	d.setUTCMilliseconds($date[i]);
		// 	$epochTime.push(d); 
		// }
		// console.log("$epochTime: "+$epochTime);
	}

	function regressionLineDataPoints($regressionLineData,$dataDates)
	{

		//$regressionLineData = [$midXAxis,$midYAxis,$gradient];
		// y = x*$gradient + $c;
		// y1 = $xStartValue*$gradient;
		// y2 = $xEndValue*$gradient;
		// $xStartValue = ($y1 - $c)/$gradient;
		// $xEndValue = ($y2 - $c)/$gradient;
		// $midYAxis = ($midXAxis - $c)/&gradient;
		// $c = $midYAxis - $midXAxis*$gradient ;
		console.log("$regressionDataPoints: $regressionLineData: " + $regressionLineData);
		// console.log("$regressionDataPoints: $xStartValue: " + $xStartValue);
		// console.log("$regressionDataPoints: $xEndValue: " + $xEndValue);
		$xArray = [];
		// $xArray.push($xStartValue);
		// $xArray.push($xEndValue);
		$xArray = convertStringDateArrayIntoMillisecondArray($dataDates);
		console.log("$xArray: " + $xArray);
		$midXAxis = $regressionLineData[0];
		$midYAxis = $regressionLineData[1];
		$gradient = $regressionLineData[2];
		$c = $midYAxis - $midXAxis*$gradient ;
		console.log("$c: " + $c);
		// // var y1 = new Date(0);
		// $y1 = $xArray[0]*$gradient + $c;
		// // var y2 = new Date(0);
		// $y2 = $xArray[1]*$gradient + $c;
		$lineData = [];
		for(var i = 0; i < $dataDates.length;i++)
		{
			$y = -($xArray[i]*$gradient + $c);
			$x = $dataDates[i];
			$coord = [$x,$y];
			$lineData.push($coord);
		}
		console.log("$lineData: " + $lineData);
		return $lineData;
	}