//the purpose of this file is to dynamically populate the 
//view with html tags depending on the state of the controller

	


    function populateItemList($data)
	{
		    // console.log("populateListExecuted!");  
			console.log($data);
			var $date = new Date();
			var $listOfItemsToView = [];
			$('#itemsList').html("");
			$listOfItemsToView.push("<table style=\"width:100%\">");
			$listOfItemsToView.push("<tr>item list!</tr>");
			$listOfItemsToView.push("<tr>");
			$listOfItemsToView.push("<th>Selection&nbsp;</th>");
			$listOfItemsToView.push("<th>&nbsp;Item Name&nbsp;</th>" );
			$listOfItemsToView.push("<th>&nbsp;Item ID&nbsp;</th>");
			$listOfItemsToView.push("</tr>");
			// console.log($listOfItemsToView);

			for(var i = 0; i < $data.length ; i++)
			{
				 $listOfItemsToView.push("<tr>");
				 $listOfItemsToView.push("<td><input type=\"checkbox\" name=\"\" value=\"\"></td>");
				 $listOfItemsToView.push("<td id =" + $data[i].item_id + " class = \"items\"><a href =\"\" >&nbsp;"+$data[i].item_name+"&nbsp;</a></td>");
				 $listOfItemsToView.push("<td>"+$data[i].item_id+"</td>");

				 $listOfItemsToView.push("</tr>");
			}

			    // $listOfItemsToView.push("<tr>");
				//  $listOfItemsToView.push("<td><input type=\"checkbox\" name=\"\" value=\"\"></td>");
				//  $listOfItemsToView.push("<td id= " + $data[1][0] + " class = \"items\"><a href =\"\" >"+$data[1][1]+"</a></td>)");
				//  $listOfItemsToView.push("<td>date</td>");
				//  $listOfItemsToView.push("</tr>");

			$listOfItemsToView.push("</table>");
			
			for(var i=0; i<$listOfItemsToView.length ;i++)
			{
				$('#itemsList').append($listOfItemsToView[i]);
			}
			// console.log($listOfItemsToView);
	}

    function populateItemListForChart()
	{
				$.ajax(
						{
							url: "datacontroller/UpdateItemList",
							type: "get", // To protect sensitive data
							data: 
							{
								ajax:true,
							},
							success: function(response)
							{
								console.log(response);
								$result = JSON.parse(response);
								// console.log($result);
								// console.log($result[0].Item_ID);
								populateItemList($result);
							}
					})
        
		// setInterval(function() {
		// 	populateItemListForChart();
		// }, 5000); //5 seconds
	}

    //function to populate user inputs for adding an item
	function populateAddItemFields()
	{	
		var addItemField = [];
		if($('#userInputAddItem').length > 0)
		{
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Item ID\" class=\"form-control\" id = \"itemID\">");			
			addItemField.push("</div>");	
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Item Name\" class=\"form-control\" id = \"itemName\">");			
			addItemField.push("</div>");			
			addItemField.push("<div class = \"form-group\">");					
			addItemField.push("<input type=\"text\" placeholder = \"Item Brand\" class=\"form-control\" id = \"itemBrand\">"); 					
			addItemField.push("</div>");			
			addItemField.push("<div class = \"form-group\">");		
			addItemField.push("<textArea cols=\"40\" rows=\"10\" placeholder = \"Item Description\" class=\"form-control\" id = \"itemDescription\"></textarea>"); 				
			addItemField.push("</div>");				
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Item Qty\" class=\"form-control\" id = \"itemQty\">");				
			addItemField.push("</div>");
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"AddItemButtonSubmit\">Add&raquo;</button>");	
			addItemField.push("</div>");
			for(var i=0; i<addItemField.length ;i++)
			{
				$('#userInputAddItem').append(addItemField[i]);
			}
             
			$('#AddItemButtonSubmit').click(function()
			{
				console.log("item submit is pressed!!");
				var itemIDVal = $('#itemID').val();
				var itemNameVal = $('#itemName').val();
				var itemBrandVal = $('#itemBrand').val();
				var itemDescriptionVal = $('#itemDescription').val();
				var itemQtyVal = $('#itemQty').val();
				$data = [itemIDVal,
						itemNameVal,
						itemBrandVal,
						itemDescriptionVal,
						itemQtyVal];
				// console.log(itemIDVal);
				// console.log(itemNameVal);
				// console.log(itemBrandVal);
				// console.log(itemDescriptionVal);
				// console.log(itemQtyVal);

				console.log("this works!!");
				$('#userInputAddItem').html('');
				$.ajax(
						{
							url: "datacontroller/AddItem",
							type: "post", // To protect sensitive data
							data: 
							{
								ajax:true,
								'itemDataInputAdd' : $data
							},
							success: function(response)
							{
								console.log("datacontroller/additem response!");
								console.log(response);
								// updateChart(dataArray, itemID);
								populateItemListForChart();
							}
					})
				return true;
			});
			return false;
		}
	}


	function populateEditItemFields()
	{	
		var editItemField = [];
		if($('#userInputEditItem').length > 0)
		{
			editItemField.push("<div class = \"form-group\">");
			editItemField.push("<input type=\"text\" placeholder = \"Item ID\" class=\"form-control\" id = \"itemID\">");			
			editItemField.push("</div>");	
			editItemField.push("<div class = \"form-group\">");
			editItemField.push("<input type=\"text\" placeholder = \"Item Name\" class=\"form-control\" id = \"itemName\">");			
			editItemField.push("</div>");			
			editItemField.push("<div class = \"form-group\">");					
			editItemField.push("<input type=\"text\" placeholder = \"Item Brand\" class=\"form-control\" id = \"itemBrand\">"); 					
			editItemField.push("</div>");			
			editItemField.push("<div class = \"form-group\">");		
			editItemField.push("<textArea cols=\"40\" rows=\"10\" placeholder = \"Item Description\" class=\"form-control\" id = \"itemDescription\"></textarea>"); 				
			editItemField.push("</div>");				
			editItemField.push("<div class = \"form-group\">");
			editItemField.push("<input type=\"text\" placeholder = \"Item Qty\" class=\"form-control\" id = \"itemQty\">");				
			editItemField.push("</div>");
			editItemField.push("<div class = \"form-group\">");
			editItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"EditItemButtonSubmit\">edit&raquo;</button>");	
			editItemField.push("</div>");
			for(var i=0; i<editItemField.length ;i++)
			{
				$('#userInputEditItem').append(editItemField[i]);
			}
             
			$('#EditItemButtonSubmit').click(function()
			{
				console.log("item submit is pressed!!");
				var itemIDVal = $('#itemID').val();
				var itemNameVal = $('#itemName').val();
				var itemBrandVal = $('#itemBrand').val();
				var itemDescriptionVal = $('#itemDescription').val();
				var itemQtyVal = $('#itemQty').val();
				$data = [itemIDVal,
						itemNameVal,
						itemBrandVal,
						itemDescriptionVal,
						itemQtyVal];
				// console.log(itemIDVal);
				// console.log(itemNameVal);
				// console.log(itemBrandVal);
				// console.log(itemDescriptionVal);
				// console.log(itemQtyVal);

				console.log("this works!!");
				$('#userInputEditItem').html('');
				$.ajax(
						{
							url: "datacontroller/EditItem",
							type: "post", // To protect sensitive data
							data: 
							{
								ajax:true,
								'itemDataInputEdit' : $data
							},
							success: function(response)
							{
								console.log("datacontroller/edititem response!");
								console.log(response);
								// updateChart(dataArray, itemID);
								populateItemListForChart();
							}
					})
				return true;
			});
			return false;
		}
	}

	function populateDeleteItemFields()
	{	
		var deleteItemField = [];
		if($('#userInputDeleteItem').length > 0)
		{
			deleteItemField.push("<div class = \"form-group\">");
			deleteItemField.push("<input type=\"text\" placeholder = \"Item ID\" class=\"form-control\" id = \"itemID\">");			
			deleteItemField.push("</div>");	
			deleteItemField.push("<div class = \"form-group\">");
			deleteItemField.push("<input type=\"text\" placeholder = \"Item Name\" class=\"form-control\" id = \"itemName\">");			
			deleteItemField.push("</div>");			
			deleteItemField.push("<div class = \"form-group\">");					
			deleteItemField.push("<input type=\"text\" placeholder = \"Item Brand\" class=\"form-control\" id = \"itemBrand\">"); 					
			deleteItemField.push("</div>");			
			deleteItemField.push("<div class = \"form-group\">");		
			deleteItemField.push("<textArea cols=\"40\" rows=\"10\" placeholder = \"Item Description\" class=\"form-control\" id = \"itemDescription\"></textarea>"); 				
			deleteItemField.push("</div>");				
			deleteItemField.push("<div class = \"form-group\">");
			deleteItemField.push("<input type=\"text\" placeholder = \"Item Qty\" class=\"form-control\" id = \"itemQty\">");				
			deleteItemField.push("</div>");
			deleteItemField.push("<div class = \"form-group\">");
			deleteItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"DeleteItemButtonSubmit\">Delete&raquo;</button>");	
			deleteItemField.push("</div>");
			for(var i=0; i<deleteItemField.length ;i++)
			{
				$('#userInputDeleteItem').append(deleteItemField[i]);
			}
             
			$('#DeleteItemButtonSubmit').click(function()
			{
				console.log("item submit is pressed!!");
				var itemIDVal = $('#itemID').val();
				var itemNameVal = $('#itemName').val();
				var itemBrandVal = $('#itemBrand').val();
				var itemDescriptionVal = $('#itemDescription').val();
				var itemQtyVal = $('#itemQty').val();
				$data = [itemIDVal,
						itemNameVal,
						itemBrandVal,
						itemDescriptionVal,
						itemQtyVal];
				// console.log(itemIDVal);
				// console.log(itemNameVal);
				// console.log(itemBrandVal);
				// console.log(itemDescriptionVal);
				// console.log(itemQtyVal);

				console.log("this works!!");
				$('#userInputDeleteItem').html('');
				$.ajax(
						{
							url: "datacontroller/DeleteItem",
							type: "post", // To protect sensitive data
							data: 
							{
								ajax:true,
								'itemDataInputDelete' : $data
							},
							success: function(response)
							{
								console.log("datacontroller/deleteitem response!");
								console.log(response);
								// updateChart(dataArray, itemID);
								populateItemListForChart();
							}
					})
				return true;
			});
			return false;
		}
	}



    function populateSellItemFields()
    {
        var addItemField = [];
        console.log("populatedSellItemFields executed!!");
		if($('#userInputSellItem').length > 0)
		{
				
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Search Item Name\" class=\"form-control\" id = \"itemNameSellItemSearch\">");			
			addItemField.push("</div>");	
            addItemField.push("<div class = \"form-group\">");
			addItemField.push("<textarea rows=\"4\" cols=\"50\" placeholder = \"Item ID\" class=\"form-control\" id = \"itemIDSellItemSearch\" readonly>");			
			addItemField.push("</div>");		
			addItemField.push("<div class = \"form-group\">");					
			addItemField.push("<textarea rows=\"4\" cols=\"50\" placeholder = \"Item Brand\" class=\"form-control\" id = \"itemBrandSellItemSearch\" readonly>"); 					
			addItemField.push("</div>");			
			addItemField.push("<div class = \"form-group\">");		
			addItemField.push("<textarea rows=\"4\" cols=\"50\" placeholder = \"Item Description\" class=\"form-control\" id = \"itemDescriptionSellItemSearch\" readonly></textarea>"); 				
			addItemField.push("</div>");				
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<textarea rows=\"4\" cols=\"50\" placeholder = \"Item Qty\" class=\"form-control\" id = \"itemQtySellItemSearch\" readonly>");				
			addItemField.push("</div>");
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"SellItemButtonSubmit\">Sell&raquo;</button>");	
			addItemField.push("</div>");
			for(var i=0; i<addItemField.length ;i++)
			{
				$('#userInputSellItem').append(addItemField[i]);
			}
             
			$('#SellItemButtonSubmit').click(function()
			{
				console.log("item submit is pressed!!");
				var itemIDVal = $('#itemIDSellItemSearch').val();
				var itemNameVal = $('#itemNameSellItemSearch').val();
				var itemBrandVal = $('#itemBrandSellItemSearch').val();
				var itemDescriptionVal = $('#itemDescriptionSellItemSearch').val();
				var itemQtyVal = $('#itemQtySellItemSearch').val();
				$data = [itemIDVal,
						itemNameVal,
						itemBrandVal,
						itemDescriptionVal,
						itemQtyVal];
				// console.log(itemIDVal);
				// console.log(itemNameVal);
				// console.log(itemBrandVal);
				// console.log(itemDescriptionVal);
				// console.log(itemQtyVal);

				console.log("this works!!");
				$('#userInputSellItem').html('');
				// $.ajax(
				// 		{
				// 			url: "datacontroller/SellItem",
				// 			type: "post", // To protect sensitive data
				// 			data: 
				// 			{
				// 				ajax:true,
				// 				'itemDataInput' : $data
				// 			},
				// 			success: function(response)
				// 			{
				// 				console.log("datacontroller/sellitem response!");
				// 				console.log(response);
				// 				// updateChart(dataArray, itemID);
				// 			}
				// 	})
				return true;
			});
			return false;
		}
    }

    // function populateCSVFileLocationInput()
    // {
    //     var addItemField = [];
	// 		addItemField.push("<div class = \"form-group\">");
	// 		addItemField.push("<input type=\"file\" placeholder = \"please insert the file location of CSV file\" class=\"form-control\" id = \"CSVFileLocation\" accept=\".csv\">");			
	// 		addItemField.push("</div>");	
	// 		addItemField.push("<div class = \"form-group\">");
	// 		addItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"ImportCSVFileButton\">Import CSV File&raquo;</button>");	
	// 		addItemField.push("</div>");

	// 		for(var i=0; i<addItemField.length ;i++)
	// 		{
	// 			$('#CSVFileLocationInput').append(addItemField[i]);
	// 		}
    // }

	function exportCSVFileLocationInput()
    {
        var addItemField = [];
		    addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Item ID\" class=\"form-control\" id = \"exportCSVItemID\">");			
			addItemField.push("</div>");
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"Item Name\" class=\"form-control\" id = \"exportCSVItemName\">");			
			addItemField.push("</div>");
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"text\" placeholder = \"please insert the file location to store CSV file\" class=\"form-control\" id = \"exportCSVFileLocation\">");			
			addItemField.push("</div>");	
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"ExportCSVFileButton\">Export CSV File&raquo;</button>");	
			addItemField.push("</div>");

			for(var i=0; i<addItemField.length ;i++)
			{
				$('#CSVFileLocationExport').append(addItemField[i]);
			}
    }

    