var files;

//Add events
// document.addEventListener("DOMContentLoaded", function(){
//     eventListeners();
// });

// function CSVEventListeners(){
//       console.log("csv event listener set");
    
// }

// //Grab the files and set them to our variable
// function prepareUpload(/*event*/)
// {
//     console.log("prepareupload working");
//     // files = event.target.files;

// }

// $('form').on('submit',uploadFiles);

// function testCSVHandler()
// {
//     console.log("csv handler is working!");
// }

// //catch the form submit and upload the files
// function uploadFiles(event)
// {
//     event.stopPropagation(); // Stop stuff happening
//     event.preventDefault();  // Totally stop stuff happening
//     var data = new FormData();
//     $each(files,function(key, value){
//         data.append(key, value);
//     });

//     $.ajax({
//         url: 'datacontroller/ProcessCSVFile()',
//         type: 'post',
//         data:data,
//         cache: false,
//         dataType: 'json',
//         processData: false, //dont process the files
//         contentType: false, // Set content type to false as jQuery will tell the server its a query string request
//         success: function(data, textStatus, jqXHR)
//         {
//             if(typeof data.error === 'undefined')
//             {
//                 //Success so call function to process the form
//                 submitForm(event,data);
//             }
//             else
//             {
//                 //Handle errors here 
//                 console.log('ERRORS: ' + data.error);
//             }
//         },
//         error: function(jqXHR, textStatus, errorThrown)
//         {
//             // Handle errors here
//             console.log('ERRORS: ' + textStatus);
//             // STOP LOADING SPINNER
//         }
//     });
// }
function populateCSVFileLocationInput()
    {
        var addItemField = [];
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<input type=\"file\" placeholder = \"please insert the file location of CSV file\" class=\"form-control\" id = \"CSVFileLocation\" accept=\".csv\">");			
			addItemField.push("</div>");	
			addItemField.push("<div class = \"form-group\">");
			addItemField.push("<button class=\"btn btn-primary btn-lg\" id=\"ImportCSVFileButton\">Import CSV File&raquo;</button>");	
			addItemField.push("</div>");

			for(var i=0; i<addItemField.length ;i++)
			{
				$('#CSVFileLocationInput').append(addItemField[i]);
			}

            
    }
//reading input CSV file
			function readCSVFile(){
				console.log("processing csv file!!");
                // $.ajaxSetup({
                //         beforeSend: function(xhr,settings){
                                var ajaxData = new FormData();
                                var file = document.getElementById('CSVFileLocation').files[0];
                                // jQuery.each($('#CSVFileLocation')[0].files,function(i,file){
                                //     ajaxData.append('file-'+ i,file)
                                // });
                                var test = "test";
                               
                                if(file){
                                    
                                    

                                    var read = new FileReader();
                                    
                                    read.onload = function(e){
                                       
                                        var contents = e.target.result;
                                        var data = [];
                                        var csvData = read.result;
                                        //  var csvData = csvData.replace(/\s+/g, "");
                                        var csvData = csvData.split(' ').join('');
                                        var rows = csvData.split("\n");
                                        //parsing data into 2d array to be sent via AJAX
                                        for (var i = 0; i< rows.length; i++)
                                        {
                                            var cells = rows[i].split(",");
                                            data.push(cells);
                                        }

                                        var parsedCSV = data;
                                         console.log(parsedCSV);
                                        // parsedCSV.forEach(function(element) {
                                        //     console.log(element);
                                        // }, this);

                                        
                                        // settings.data += '&file=' + btoa(contents);
                                       
                                        //send data to controller
                                         $.ajax(
                                                {
                                                    url: "ControlPanel/ProcessCSVFile",
                                                    type: "POST", // To protect sensitive data
                                                    // processData: false,
                                                    // contentType: false,
                                                    data: 
                                                    {
                                                        ajax:true,
                                                        'test':test,
                                                        'parsedCSV' : parsedCSV                                                    },
                                                    success: function(response)
                                                    {
                                                    //    console.log(JSON.parse(response));
                                                       $('#CSVFileLocationInput').append(response);
                                                       $('#which_sheet_CSV').click(function(){
                                                           console.log($('#which_sheet').val());
                                                        $.ajax({
                                                            url:"ControlPanel/populateDatabaseWithCSV",
                                                            type: "POST",
                                                            data:
                                                            {
                                                                which_sheet:$('#which_sheet').val(),
                                                                'parsedCSV' : parsedCSV   
                                                            },
                                                            success: function (response)
                                                            {
                                                                console.log("uploaded document");
                                                            }
                                                        })


                                                       });
                                                    },
                                                    error: function(e){
                                                         console.log('Error: ' + e);                       
                                                    }
                                            })
                                        
                                    }
                                    read.readAsText(file);
                                } else {
                                    console.log("failed to load file");
                                }
                        // }
                    // });
				}
