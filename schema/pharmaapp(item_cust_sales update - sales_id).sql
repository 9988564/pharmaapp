-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2017 at 08:10 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pharmaapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `item_id` int(10) NOT NULL AUTO_INCREMENT,
  `item_name` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `qty` int(5) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_description`, `brand`, `qty`) VALUES
(2, 'Paracetemol', 'Pain Killer', 'Panadol', 900),
(3, 'something', 'something', 'something', 6),
(4, 'something', 'something', 'something', 324),
(5, 'sp2', 'sp2', 'sp2', 400);

-- --------------------------------------------------------

--
-- Table structure for table `item_cust_sales`
--

CREATE TABLE IF NOT EXISTS `item_cust_sales` (
  `item_id` int(11) DEFAULT NULL,
  `datetime_sale` datetime NOT NULL,
  `qty_of_sale` int(10) NOT NULL,
  `sales_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`sales_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `item_cust_sales`
--

INSERT INTO `item_cust_sales` (`item_id`, `datetime_sale`, `qty_of_sale`, `sales_id`) VALUES
(2, '2017-01-13 15:00:00', 900, 1),
(2, '2017-01-14 15:00:00', 700, 2),
(2, '2017-01-15 15:00:00', 800, 3),
(2, '2017-01-16 15:00:00', 500, 4),
(2, '2017-01-17 15:00:00', 600, 5),
(2, '2017-01-18 15:00:00', 400, 6),
(2, '2017-01-19 15:00:00', 350, 7),
(2, '2017-01-10 15:00:00', 200, 8),
(2, '2017-01-21 15:00:00', 100, 9),
(2, '2017-01-22 15:00:00', 150, 10);

-- --------------------------------------------------------

--
-- Table structure for table `item_stock_dates`
--

CREATE TABLE IF NOT EXISTS `item_stock_dates` (
  `item_id` int(10) NOT NULL,
  `date_purchased` datetime NOT NULL,
  `est_restock_date` datetime NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_stock_dates`
--

INSERT INTO `item_stock_dates` (`item_id`, `date_purchased`, `est_restock_date`) VALUES
(2, '2017-01-12 00:00:00', '2017-01-20 17:00:00'),
(3, '2017-05-25 04:44:00', '2017-05-13 16:07:00'),
(4, '2017-05-18 11:11:00', '2017-05-21 11:11:00'),
(5, '2014-05-05 11:11:00', '2014-06-06 23:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet`
--

CREATE TABLE IF NOT EXISTS `spreadsheet` (
  `spreadsheet_id` int(8) NOT NULL AUTO_INCREMENT,
  `spreadsheet_name` varchar(255) NOT NULL,
  `spreadsheet_description` varchar(255) NOT NULL,
  PRIMARY KEY (`spreadsheet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `spreadsheet`
--

INSERT INTO `spreadsheet` (`spreadsheet_id`, `spreadsheet_name`, `spreadsheet_description`) VALUES
(21, 'Spreadsheet 1 (TestUser)', 'Spreadsheet 1 Desc (TestUser)'),
(22, 'Spreadsheet 2 (TestUser)', 'Spreadsheet 2 Desc (TestUser)'),
(23, 'Swinburne Spreadsheet 1', 'Swinburne Spreadsheet 1 Desc'),
(24, 'sample_Purchases', 'sample_Purchases'),
(25, 'test', 'test'),
(26, 'new spreadsheet', 'alsfkjaskldfjalkf');

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet_items`
--

CREATE TABLE IF NOT EXISTS `spreadsheet_items` (
  `spreadsheet_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  PRIMARY KEY (`spreadsheet_id`,`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spreadsheet_items`
--

INSERT INTO `spreadsheet_items` (`spreadsheet_id`, `item_id`) VALUES
(21, 2),
(21, 3),
(21, 4),
(22, 5);

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet_user`
--

CREATE TABLE IF NOT EXISTS `spreadsheet_user` (
  `id` int(4) NOT NULL,
  `spreadsheet_id` int(4) NOT NULL,
  PRIMARY KEY (`id`,`spreadsheet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spreadsheet_user`
--

INSERT INTO `spreadsheet_user` (`id`, `spreadsheet_id`) VALUES
(1, 23),
(2, 21),
(2, 22),
(2, 24),
(2, 25),
(2, 26);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'swinburne', '5f4dcc3b5aa765d61d8327deb882cf99'),
(2, 'testuser', '8eee3efdde1eb6cf6639a58848362bf4');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
