-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2017 at 11:09 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pharmaapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `item_id` int(10) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_description` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `qty` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`item_id`, `item_name`, `item_description`, `brand`, `qty`) VALUES
(2, 'Paracetemol', 'Pain Killer', 'Panadol', 900);

-- --------------------------------------------------------

--
-- Table structure for table `item_cust_sales`
--

CREATE TABLE `item_cust_sales` (
  `item_id` int(10) NOT NULL,
  `datetime_sale` datetime NOT NULL,
  `qty_of_sale` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_cust_sales`
--

INSERT INTO `item_cust_sales` (`item_id`, `datetime_sale`, `qty_of_sale`) VALUES
(2, '2017-01-13 15:00:00', 100);

-- --------------------------------------------------------

--
-- Table structure for table `item_stock_dates`
--

CREATE TABLE `item_stock_dates` (
  `item_id` int(10) NOT NULL,
  `date_purchased` datetime NOT NULL,
  `est_restock_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_stock_dates`
--

INSERT INTO `item_stock_dates` (`item_id`, `date_purchased`, `est_restock_date`) VALUES
(2, '2017-01-12 00:00:00', '2017-01-20 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet`
--

CREATE TABLE `spreadsheet` (
  `spreadsheet_id` int(8) NOT NULL,
  `spreadsheet_name` varchar(255) NOT NULL,
  `spreadsheet_description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spreadsheet`
--

INSERT INTO `spreadsheet` (`spreadsheet_id`, `spreadsheet_name`, `spreadsheet_description`) VALUES
(21, 'Spreadsheet 1 (TestUser)', 'Spreadsheet 1 Desc (TestUser)'),
(22, 'Spreadsheet 2 (TestUser)', 'Spreadsheet 2 Desc (TestUser)'),
(23, 'Swinburne Spreadsheet 1', 'Swinburne Spreadsheet 1 Desc');

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet_items`
--

CREATE TABLE `spreadsheet_items` (
  `spreadsheet_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spreadsheet_items`
--

INSERT INTO `spreadsheet_items` (`spreadsheet_id`, `item_id`) VALUES
(21, 2);

-- --------------------------------------------------------

--
-- Table structure for table `spreadsheet_user`
--

CREATE TABLE `spreadsheet_user` (
  `id` int(4) NOT NULL,
  `spreadsheet_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `spreadsheet_user`
--

INSERT INTO `spreadsheet_user` (`id`, `spreadsheet_id`) VALUES
(1, 23),
(2, 21),
(2, 22);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` tinyint(4) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'swinburne', '5f4dcc3b5aa765d61d8327deb882cf99'),
(2, 'testuser', '8eee3efdde1eb6cf6639a58848362bf4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_cust_sales`
--
ALTER TABLE `item_cust_sales`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `item_stock_dates`
--
ALTER TABLE `item_stock_dates`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `spreadsheet`
--
ALTER TABLE `spreadsheet`
  ADD PRIMARY KEY (`spreadsheet_id`);

--
-- Indexes for table `spreadsheet_items`
--
ALTER TABLE `spreadsheet_items`
  ADD PRIMARY KEY (`spreadsheet_id`,`item_id`);

--
-- Indexes for table `spreadsheet_user`
--
ALTER TABLE `spreadsheet_user`
  ADD PRIMARY KEY (`id`,`spreadsheet_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `spreadsheet`
--
ALTER TABLE `spreadsheet`
  MODIFY `spreadsheet_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
