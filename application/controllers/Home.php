<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// session_start(); //we need to start session in order to access it through CI

class Home extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		// Load form helper library
		$this->load->helper('form');

		// Load form validation library
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}
}
