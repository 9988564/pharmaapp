<?php 
//Derived from http://www.iluv2code.com/login-with-codeigniter-php.html

class Login extends CI_Controller 
{
 
    function __construct()
    {
        parent::__construct();
        $this->load->model('user','',TRUE);

        // Load form helper library
        $this->load->helper('form');

        // Load form validation library
        $this->load->library('form_validation');
    }
 
    function index()
    {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            //Field validation failed.  User redirected to login page
        $this->load->view('header');
        $this->load->view('auth_fail');
        $this->load->view('footer');
        }
        else
        {
            //Go to private area
            redirect('ControlPanel', 'refresh');
        }
    }

    function check_database($password)
    {
        $username = $this->input->post('username');
        $result = $this->user->login($username, $password);

        if($result)
        {
            $sess_array = array();
            foreach($result as $row)
            {
                $sess_array = array('id' => $row->id, 'username' => $row->username);
                $this->session->set_userdata('logged_in', $sess_array);
            }
            
            return true;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        session_destroy();
        redirect('Home', 'refresh');
    }   
}







?>