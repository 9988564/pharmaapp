<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once($_SERVER['DOCUMENT_ROOT'] . '/PharmaApp/assets/php/Item.php');

class ControlPanel extends CI_Controller 
{
    private $userID = 0;
    private $userName = "";
    private $loggedIn = false;

	public function __construct() 
	{
		parent::__construct();
		$this->load->model('ControlPanelmodel');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');

        //Check if user is logged in:
        if($this->session->userdata('logged_in'))
		{
			$session_data = $this->session->userdata('logged_in');
            $this->userID = $session_data['id'];
            $this->userName = $session_data['username'];
            $this->loggedIn = true;
		}
        else
        {
            redirect('Login', 'refresh');
        }
	}

    private function IsLoggedIn()
    {
        return ($this->session->userdata('logged_in') && $this->loggedIn == true);
    }

	public function index()
	{
        if ($this->IsLoggedIn())
        {
            $this->load->view('header');
		    $this->load->view('usercontrolpanel');
		    $this->load->view('footer');
        }
	}

    public function GetSpreadSheets()
    {
        $results = $this->ControlPanelmodel->GetSpreadSheets($this->userID);
        echo $results;
    }

    public function CreateSpreadSheet()
    {
        $sheet_name = trim($_POST["sheet_name"]);
        $sheet_desc = trim($_POST["sheet_desc"]);

        //Check if we were successful in adding the spreadsheet.
        if ($this->ControlPanelmodel->AddSpreadSheet($this->userID, $sheet_name, $sheet_desc))
        {
            echo "<p>Nice! The spreadsheet was added. Rest assured, only you can see this spreadsheet.<br>Now, you can create some more spreadsheets, or use the select above to view existing sheets or add items.</p>";
        }
        else
        {
            echo "<p>Unfortunately, this spreadsheet wasn't able to be created. Please try again or contact the administrator.</p>";
        }
    }

    public function InsertItemToSpreadSheet()
    {
        //Create the item:
        $item = new Item($_POST["which_sheet"], $_POST["item_name"], $_POST["item_desc"], $_POST["brand_name"], $_POST["item_qty"], $_POST["purchase_date"], $_POST["est_restock_date"], $_POST["sale_date"], $_POST["sale_qty"]);
        //Check if we were successful in adding the item.
        if ($this->ControlPanelmodel->AddItem($item))
        {
            echo "<p>This item was added successfully to the selected spreadsheet.</p>";
        }
        else
        {
            echo "<p>An error occurred and the item could not be added. Please try again.</p>";
        }

    }

    public function ShowAddSpreadsheetForm()
    {
        ?>
        <form id="add-spreadsheet-form" method="post" accept-charset="utf-8">
            <label style="width:200px" for="sheet_name">Spreadsheet Name: </label>
            <input type="text" name="sheet_name" value="" id="sheet_name" maxlength="100" size="50" style="width:50%" required>
            <br>
            <label style="width:200px" for="sheet_desc">Spreadsheet Description: </label>
            <input type="text" name="sheet_desc" value="" id="sheet_desc" maxlength="100" size="50" style="width:50%" required>
            <br>
            <input type="submit" value="Add SpreadSheet">
        </form>
        <?php
    }

    public function ShowAddItemToExistingForm()
    {
        ?>
        <form id="insert-item-form" method="post" accept-charset="utf-8">
            <label style="width:200px" for="which_sheet">Add To This Spreadsheet: </label>
            <select name="which_sheet" id="which_sheet" style="width:50%" required>
            <option selected="selected">Choose one</option>
            <?php
                foreach($this->ControlPanelmodel->GetSpreadSheetAsArray($this->userID) as $name) { ?>
                <option value="<?= explode("<><>", $name)[1]; ?>"><?= explode("<><>", $name)[0]; ?></option>
            <?php
                } ?>
            </select> 
            <br>
            <br>
            <label style="width:200px" for="item_name">Item Name: </label>
            <input type="text" name="item_name" value="" id="item_name" maxlength="100" size="50" style="width:50%" required>
            <br>
            <label style="width:200px" for="item_desc">Item Description: </label>
            <input type="text" name="item_desc" value="" id="item_desc" maxlength="100" size="50" style="width:50%" required>
            <br>
            <label style="width:200px" for="brand_name">Brand: </label>
            <input type="text" name="brand_name" value="" id="brand_name" maxlength="100" size="50" style="width:50%" required>
            <br>
            <label style="width:200px" for="item_qty">Quantity: </label>
            <input type="number" name="item_qty" value="" id="item_qty" maxlength="100" size="50" style="width:50%" required>
            <br>
            <label style="width:200px" for="purchase_date">Date of Purchase: </label>
            <input type="datetime-local" name="purchase_date" value="" id="purchase_date" style="width:50%; margin-bottom:2px;" required>
            <br>
            <label style="width:200px" for="est_restock_date">Estimated Restock Date: </label>
            <input type="datetime-local" name="est_restock_date" value="" id="est_restock_date" style="width:50%; margin-bottom:2px;" required>
            <br>
            <label style="width:200px" for="sale_date">Date of Sale: </label>
            <input type="datetime-local" name="sale_date" value="" id="sale_date" style="width:50%; margin-bottom:2px;" required>
            <br>
            <label style="width:200px" for="sale_qty">Quantity of Sale: </label>
            <input type="number" name="sale_qty" value="" id="sale_qty" style="width:50%" required>
            <br>
            <br>


            <input type="submit" value="Insert into Selected Spreadsheet">
        </form>
        <?php
    }

    public function ViewSpreadSheet()
    {
        // die(var_dump($_POST));
        $id =  $_POST["spreadsheet_id"];
        // echo $id;
        echo $this->ControlPanelmodel->GetSpreadSheet($id);
    }

    public function ProcessCSVFile()
	{
		$input = $_POST['parsedCSV'];
        
            ?>
            <label style="width:200px" for="which_sheet">Add To This Spreadsheet: </label>
            <select name="which_sheet" id="which_sheet" style="width:50%" required>
            <option selected="selected">Choose one</option>
            <?php
                foreach($this->ControlPanelmodel->GetSpreadSheetAsArray($this->userID) as $name) { ?>
                <option value="<?= explode("<><>", $name)[1]; ?>"><?= explode("<><>", $name)[0]; ?></option>
            <?php
                } ?>
            </select>
            <button type="button" id = "which_sheet_CSV">upload CSV</button>
            <?php 
        
      


        echo $this->ControlPanelmodel->populateCSVTable($input);
        

        
        // $result = $this->ControlPanelmodel->importCSVFile($this->userID,$input);
		// $result = json_encode($input);
		// echo $result;
	}


    public function populateDatabaseWithCSV()
    {
        $sheet = $_POST("which_sheet");
        $CSVData = $_POST("parsedCSV");

          for($i = 1; $i < count($CSVData); $i++ )
        {
            // $item = new Item($_POST["which_sheet"], $_POST["item_name"], $_POST["item_desc"], $_POST["brand_name"], $_POST["item_qty"], $_POST["purchase_date"], $_POST["est_restock_date"], $_POST["sale_date"], $_POST["sale_qty"]);
            $item = new Item($_POST["which_sheet"], $CSVData[i][1], $CSVData[i][2],$CSVData[i][3],$CSVData[i][4], $CSVData[i][5],$CSVData[i][6], $CSVData[i][7],"Null","Null");
            if ($this->ControlPanelmodel->AddItem($item))
            {
                echo "<p>This item was added successfully to the selected spreadsheet.</p>";
            }
            else
            {
                echo "<p>An error occurred and the item could not be added. Please try again.</p>";
            }
        }

    }

    public function populateBurnDownChart()
    {
        echo json_encode($this->ControlPanelmodel->populateBurnDownChart($_POST["Bspreadsheet_id"]));
    }

    public function ViewSales()
    {
        $itemArray = $this->ControlPanelmodel->GetSalesArray($_POST["spreadsheet_id"], $_POST["item_id"]);
        $itemTable = $this->ControlPanelmodel->GetSales($_POST["spreadsheet_id"], $_POST["item_id"]);
        $ArrayBuffer = array($itemTable,$itemArray);
        echo json_encode($ArrayBuffer);
        // echo $this->ControlPanelmodel->GetSales($_POST["spreadsheet_id"], $_POST["item_id"]);
    
    }
}
