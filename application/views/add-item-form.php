<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>


<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/datamodelajax.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/highcharts.js"></script>
<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-6 padding-col">
				<h1>This is the page for item form entry</h1>
				<p><button class="btn btn-primary btn-lg" id="ViewAllUploadedDocumentsButton">View All Uploaded Documents &raquo;</button></p>
				<p><button class="btn btn-primary btn-lg" id="ViewAllCreatedDiagramsButton">View All Created Diagrams &raquo;</button></p>
				<p><button class="btn btn-primary btn-lg" id="UploadDocumentButton">Upload Document &raquo;</button></p>
				<p><button class="btn btn-primary btn-lg" id="CreateDiagramButton">Create Diagram &raquo;</button></p>
			</div>
			<div class="col-md-6 padding-col">
				<h1>Below you can see and select documents/diagrams.</h1>
				<p>(Hide documents & document buttons when on diagrams and vice versa)</p>
				<p>
					<button class="btn btn-primary btn-lg" id="DownloadAllDiagramsButton">Download All Diagrams &raquo;</button>
					<button class="btn btn-primary btn-lg" id="DeleteAllDiagramsButton">Delete All Diagrams &raquo;</button>
				</p>
				<p>
					<button class="btn btn-primary btn-lg" id="DownloadAllDocumentsButton">Download All Documents &raquo;</button>
					<button class="btn btn-primary btn-lg" id="DeleteAllDocumentsButton">Delete All Documents &raquo;</button>
					<button class="btn btn-primary btn-lg" id="AddItemButton">Add New Item &raquo;</button>
					<button class="btn btn-primary btn-lg" id="SearchItemButton">Search For Item &raquo;</button>
				</p>
				<br>
				<p>This is just an example. Don't hardcode any tables. Use either PHP or JQuery to dynamically create the tables based on selection.</p>
				<br>
				<table style="width:100%" id = "container">
					<tr>
						<th>Selection</th>
						<th>Item Name</th> 
						<th>Date Created</th>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id1" class = "items"><a href ="" >Item 1</a></td> 
						<td>1/1/1975</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id2" class = "items"><a href ="">Item 2</a></td> 
						<td>1/1/1980</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id3" class = "items"><a href ="">Item 3</a></td> 
						<td>1/1/1985</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id4" class = "items"><a href ="">Item 4</a></td> 
						<td>1/1/1990</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id5" class = "items"><a href ="">Item 5</a></td> 
						<td>1/1/1995</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id6" class = "items"><a href ="">Item 6</a></td> 
						<td>1/1/2000</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id7" class = "items"><a href ="">Item 7</a></td> 
						<td>1/1/2005</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id8" class = "items"><a href ="">Item 8</a></td> 
						<td>1/1/2010</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id9" class = "items"><a href ="">Item 9</a></td> 
						<td>1/1/2015</td>
					</tr>
					<tr>
						<td><input type="checkbox" name="" value=""></td>
						<td id = "id10" class = "items"><a href ="">Item 10</a></td> 
						<td>1/1/2020</td>
					</tr>
					</table>

					

			</div>
			<div class="col-md-6 padding-col">
				<h1>Burn Down Chart</h1>
				<div id = "CSV-Burn-Down-Chart">
				</div>
			</div>
		</div>