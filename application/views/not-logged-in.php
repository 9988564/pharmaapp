<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Private: You are not logged in.</h1>
                <p>The page you are trying to enter is private.</p>
                <p>You cannot access this page unless you log in. To log in, enter your username and password on the header bar at the top of the page.</p>
                <?php echo validation_errors('<div class="error">', '</div>'); ?>
			</div>
		</div>
    </div>