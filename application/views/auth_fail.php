<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Oh no, login failed!</h1>
                <p>Oh no, the login has failed! Please review the following errors, and then try again:</p>
                <?php echo validation_errors('<div class="error">', '</div>'); ?>
			</div>
		</div>
    </div>