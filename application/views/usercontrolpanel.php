<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/dynamicview.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/BurnDownChartHandler.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/CSVHandler.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/js/user.js" ></script>



<div class="">
	<div class="container">

		<!-- WHICH ACTION? -->
		<div class="row">
			<div class="col-md-12 padding-col-small">
				<h1>Welcome to PharmaApp.</h1>
				<p class="main-query">Please choose an action that you would like to perform:</p>
				<select class="main-selector">
					<option value="nil" selected>Choose One</option>
					<option value="view">View Existing Spreadsheets</option>
					<option value="new">Create New Spreadsheet</option>
					<option value="add">Add to Existing Spreadsheet</option>
				</select>
			</div>
		</div>

		<!-- VIEW EXISTING SPREADSHEETS -->
		<div class="row" id="view-existing-container">
			<div class="col-md-12 padding-col-small">
				<p id="view-existing-main-query">Here is the list of all spreadsheets on our system, for your user ID.<br> If you want more information, just click on its name.</p>
				<div id="view-existing">
				</div>
				<div id="the-spreadsheet">
				</div>
			</div>
		</div>

		<!-- CREATE NEW SPREADSHEET -->
		<div class="row" id="create-new-container">
			<div class="col-md-12 padding-col-small">
				<p id="create-new-main-query">You're going to create a new spreadsheet. Please enter the following details:</p>
				<div id="create-new">
				</div>
			</div>
		</div>

		<!-- ADD TO EXISTING SPREADSHEET -->
		<div class="row" id="add-to-existing-container">
			<div class="col-md-12 padding-col-small">
				<p id="add-to-existing-main-query">
					Please fill out the following form, remembering to take care to ensure it's added to the correct spreadsheet.<br>
					Note: This form assumes you are adding a NEW item, which you have already stocked AND already sold.<br>
					If you want to add new data for an EXISTING item, click here.
				</p>
				<div id="add-to-existing">
				</div>
			</div>
		</div>