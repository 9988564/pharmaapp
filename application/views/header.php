<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE HTML>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets'); ?>/css/main.css">
        <script src="<?php echo base_url('assets'); ?>/js/vendor/jquery-1.11.2.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/plugins.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/main.js"></script>
        <script src="<?php echo base_url('assets'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">PharmaApp</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <?php
          //Check if we're logged in - if we are, display welcome message. If we're not, display log in form.
          if($this->session->userdata('logged_in'))
          {
            $session_data = $this->session->userdata('logged_in');
            $data['username'] = $session_data['username'];
            ?>

            <?php echo form_open('Login/logout', array('class' => 'navbar-form navbar-right')); ?>
            <button type="submit" class="btn btn-success">Log Out</button>
            <?php echo form_close(); ?>
            <?php
          }
          else
          {
            ?>
            <?php echo form_open('Login', array('class' => 'navbar-form navbar-right')); ?>
              <div class="form-group">
                <input type="text" placeholder="Username" id="username" name="username" class="form-control">
              </div>
              <div class="form-group">
                <input type="password" placeholder="Password" id="password" name="password" class="form-control">
              </div>
              <button type="submit" class="btn btn-success">Sign in</button>
            <?php echo form_close(); ?>
            <?php
          }
          ?>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>