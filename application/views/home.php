<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>


<div class="">
	<div class="container">
		<div class="row">
			<div class="col-md-8 padding-col">
				<h1>Welcome to PharmaApp</h1>
				<p>This website is designed to help pharmacies across Australia simplify their lives by keeping track of stock and automating the record keeping process.</p>
				<p>To use PharmaApp, please log in on the mainmenu to the top of the page.</p>
				<p>If you do not yet have a log in, please contact us using the button below to enquire about purchasing a license to use PharmaApp.</p>
				<br><br>
				<p><a class="btn btn-primary btn-lg" href="#" role="button">Contact Us Now &raquo;</a></p>
			</div>
			<div class="col-md-4 padding-col">
				<img id="pharmacy-photo" src="<?php echo base_url('assets'); ?>/images/pharmacy.jpg">
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 padding-col">
				<h2>Why use PharmaApp?</h2>
				<p>PharmaApp is used by hundreds of pharmacists worldwide for one reason: Simplicity. Long gone are the days of ledgers or manual spreadsheets.</p>
			</div>
			<div class="col-md-6 padding-col">
				<h2>How to use PharmaApp?</h2>
				<p>Simply log in at the top. If you don't have login details, call us. After logging in, the functions will appear on the mainmenu and on the user page.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 padding-col">
				<h1 class="text-center">What are you waiting for? Let's get started!<h1>
			</div>
		</div>