<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/PharmaApp/assets/php/Item.php');


class ControlPanelModel extends CI_Model {

	public $template;

	public function __construct()
	{
		parent::__construct();

        $this->load->library('table');

		$this->template = array(
				'table_open'            => '<table border="1" style="width:100%;" class="spreadsheet-list">',

				'thead_open'            => '<thead>',
				'thead_close'           => '</thead>',

				'heading_row_start'     => '<tr>',
				'heading_row_end'       => '</tr>',
				'heading_cell_start'    => '<th style="padding:10px;">',
				'heading_cell_end'      => '</th>',

				'tbody_open'            => '<tbody>',
				'tbody_close'           => '</tbody>',

				'row_start'             => '<tr>',
				'row_end'               => '</tr>',
				'cell_start'            => '<td style="padding:10px;">',
				'cell_end'              => '</td>',

				'row_alt_start'         => '<tr>',
				'row_alt_end'           => '</tr>',
				'cell_alt_start'        => '<td style="padding:10px;">',
				'cell_alt_end'          => '</td>',

				'table_close'           => '</table>'
		);

		$this->table->set_template($this->template);

			
	}

	public function AddItem($item)
	{
		//First, we need to start a transaction.
		$this->db->trans_start();

		//Then, we get our item:
		$itemInsert = $item->GetItem();

		//Assign the spreadsheet this item should go to:
		$linkedSheet = $itemInsert["which_sheet"];

		//Now, we insert the item:
		$this->db->insert('items', array(
			'item_name' => $itemInsert["item_name"],
			'item_description' => $itemInsert["item_desc"],
			'brand' => $itemInsert["brand_name"],
			'qty' => $itemInsert["item_qty"]
			)
		);

		//Now, we need to get that ID for future inserts:
		$theID = $this->db->insert_id();

		//Then we need to insert our purchase:
		$itemInsert = $item->GetPurchase();
		$this->db->insert('item_stock_dates', array(
			'item_id' => $theID,
			'date_purchased' => $itemInsert["purchase_date"],
			'est_restock_date' => $itemInsert["est_restock_date"]
			)
		);

		//Now, we need to insert our sale:
		$itemInsert = $item->GetSale();
		$this->db->insert('item_cust_sales', array(
			'item_id' => $theID,
			'datetime_sale' => $itemInsert["sale_date"],
			'qty_of_sale' => $itemInsert["sale_qty"]
			)
		);

		//Now, we have to insert this item in to our spreadsheet:
		$this->db->insert('spreadsheet_items', array(
			'spreadsheet_id' => $linkedSheet,
			'item_id' => $theID
			)
		);


		//Complete the transaction.
		$this->db->trans_complete();

		//Check success.
		if ($this->db->trans_status() === false)
		{
			return false;
		}

		return true;
	}

	public function GetSpreadSheets($userID)
	{
		$query = $this->db->query("
			SELECT s.spreadsheet_id, s.spreadsheet_name, s.spreadsheet_description FROM `spreadsheet` s
			INNER JOIN `spreadsheet_user` su
			ON s.spreadsheet_id = su.spreadsheet_id
			where su.id = $userID");

		$this->table->set_heading('Name', 'Description');
		

		foreach ($query->result() as $row)
		{
			$name = "<a href='' id='spreadsheet-$row->spreadsheet_id' class='spreadsheet-view-button'>" . $row->spreadsheet_name . "</a>";
			$desc = $row->spreadsheet_description;

			$this->table->add_row($name, $desc);
		}


		$table = $this->table->generate();
		return $table;
	}

	public function GetSpreadSheetAsArray($userID)
	{
		$arr = array();

		$query = $this->db->query("
			SELECT s.spreadsheet_id, s.spreadsheet_name, s.spreadsheet_description FROM `spreadsheet` s
			INNER JOIN `spreadsheet_user` su
			ON s.spreadsheet_id = su.spreadsheet_id
			where su.id = $userID");

		foreach ($query->result() as $row)
		{
			array_push($arr, $row->spreadsheet_name . ' <><>' . $row->spreadsheet_id);
		}

		return $arr;
	}

	public function AddSpreadSheet($userID, $sheet_name, $sheet_desc)
	{
		//We need to insert to multiple tables, therefore we need it to be atomic - so let's use a transaction.
		$this->db->trans_start();

		//Insert #1.
		$this->db->insert('spreadsheet', array('spreadsheet_name' => $sheet_name, 'spreadsheet_description' => $sheet_desc));

		// //Insert #2.
		$this->db->insert('spreadsheet_user', array('id' => $userID, 'spreadsheet_id' => $this->db->insert_id()));

		//Complete the transaction.
		$this->db->trans_complete();

		//Check success.
		if ($this->db->trans_status() === false)
		{
			return false;
		}

		return true;
	}

	public function GetSpreadSheet($spreadsheet_id)
	{
		// echo $spreadsheet_id;
		$query = $this->db->query("
			SELECT i.item_name as `Item Name`, i.item_description as `Item Description`, i.brand as `Brand`, i.qty as `Current Stock Quantity`, 
			CONCAT('<button type=\'button\'', 'id=\'button_', s.spreadsheet_id, '_', i.item_id, '\' class=\'btn btn-success view-sales-button\'', 'data-item_id=\'', i.item_id, '\'', 'data-spreadsheet_id=\'', s.spreadsheet_id, '\'', '>View Sales</button>') as ``
			FROM spreadsheet s
			INNER JOIN spreadsheet_items si
			ON s.spreadsheet_id = si.spreadsheet_id
			INNER JOIN items i
			ON i.item_id = si.item_id
			WHERE s.spreadsheet_id = $spreadsheet_id"
		);

		//Append button to the last column of each row, to view the sales.
		$table = $this->table->generate($query);

		return $this->table->generate($query);
	}

	public function GetSales($spreadsheet_id, $item_id)
	{
		$query = $this->db->query("
			SELECT i.item_name as `Item Name`, i.item_description as `Item Description`, i.brand as `Brand`, ics.datetime_sale as `Date of Sale`, ics.qty_of_sale as `Quantity of Sale`
			FROM spreadsheet s
			INNER JOIN spreadsheet_items si
			ON s.spreadsheet_id = si.spreadsheet_id
			INNER JOIN items i
			ON i.item_id = si.item_id
			INNER JOIN item_cust_sales ics
			ON ics.item_id = i.item_id
			WHERE i.item_id = $item_id AND s.spreadsheet_id = $spreadsheet_id"
		);

		//Append button to the last column of each row, to view the sales.
		$table = $this->table->generate($query);

		return $this->table->generate($query);
	}

	public function populateBurnDownChart($spreadsheet_id)
	{
		$query = $this->db->query("
			SELECT i.item_name as `Item Name`, i.item_description as `Item Description`, i.brand as `Brand`, i.qty as `Quantity of Stock`, isd.date_purchased as `Date Purchased`, isd.est_restock_date as `Estimated Restock Date`, ics.datetime_sale `Date of Sale`, ics.qty_of_sale as `Sale Quantity` from spreadsheet s
			INNER JOIN spreadsheet_items si
			ON s.spreadsheet_id = si.spreadsheet_id
			INNER JOIN items i
			ON i.item_id = si.item_id
			INNER JOIN item_cust_sales ics
			ON ics.item_id = si.item_id
			INNER JOIN item_stock_dates isd
			ON isd.item_id = si.item_id
			WHERE s.spreadsheet_id = $spreadsheet_id"
		);
		$array = $query->result_array();
		//  $this->table->generate($query);
		return $array;
	}

	public function importCSVFile($userID,$input)
	{


		AddItem($item);
		/*
		* create an insert statement for each row
		*/

		//trim whitespaces from data
		$dataTrimed = array();
		for($i = 0; $i < count($data); $i++)
		{
			array_push($dataTrimed,array_map('trim',$data[$i]));
		}
    	
		//store column names
		$columnNames = array();
		array_push($columnNames,$dataTrimed[0]);
		$dataBuffer = array();
		$sqlArray = array();

		
		//separate data from the column titles
		for($i = 1; $i < count($dataTrimed);$i++)
		{
			array_push($dataBuffer,$dataTrimed[$i]);
		}

		//create insert statements
		for($k = 0; $k < count($dataBuffer); $k++)
		{
				$sql = '';
				$sql .= "INSERT INTO `pharmaapp`.`purchasecsv`(";
					for($i = 0; $i < count($columnNames); $i++)
					{
						for($j = 0; $j <count($columnNames[$i]); $j++)
						{
						$sql .= "`";
						$sql .= $columnNames[$i][$j];
						$sql .= "`";
							if($j < count($columnNames[$i]) - 1)
							{
							$sql .= ", ";
							}
						
						}
					}
					$sql .= ") VALUES (";
					for($i = 0; $i < count($dataBuffer[$k]); $i++)
					{
					$sql .= "'";
					$sql .= $dataBuffer[$k][$i];
					$sql .= "'";
					if($i < count($dataBuffer[$k]) - 1)
					{
					$sql .= ", ";
					}
					
				}
				$sql .=");";
				
				$sql = trim(preg_replace('/\s+/', ' ', $sql));	//remove new lines that create double spaces
				$query = $this->db->query($sql);
				array_push($sqlArray,$sql);
		}
		return $sqlArray;	//return sql array for feedback
	}

	

	public function populateCSVTable($csvParsedArray)
	{
		// $this->table->set_heading('item_id', 'item_name', 'item_description', 'brand', 'qty', 'date purchased', 'est restock date');
		// for($i = 1; $i < count($csvParsedArray); $i++)
		// {
		// 	// $name = "<a href='' id='spreadsheet-$row->spreadsheet_id' class='spreadsheet-view-button'>" . $row->spreadsheet_name . "</a>";
		// 	// $desc = $row->spreadsheet_description;

		// 	$this->table->add_row($csvParsedArray[i][0], $csvParsedArray[i][1],$csvParsedArray[i][2],$csvParsedArray[i][3],$csvParsedArray[i][4],$csvParsedArray[i][5],$csvParsedArray[i][6]);
		// }

		// $table = $this->table->generate();

		// $this->load->library('table');

			$data = $csvParsedArray;
			echo $this->table->generate($data);
		// return $table;
	}

	public function GetSalesArray($spreadsheet_id, $item_id)
	{
		$query = $this->db->query("
			SELECT i.item_name as `Item Name`, i.item_description as `Item Description`, i.brand as `Brand`, ics.datetime_sale as `Date of Sale`, ics.qty_of_sale as `Quantity of Sale`
			FROM spreadsheet s
			INNER JOIN spreadsheet_items si
			ON s.spreadsheet_id = si.spreadsheet_id
			INNER JOIN items i
			ON i.item_id = si.item_id
			INNER JOIN item_cust_sales ics
			ON ics.item_id = i.item_id
			WHERE i.item_id = $item_id AND s.spreadsheet_id = $spreadsheet_id"
		);

		//Append button to the last column of each row, to view the sales.
		$array = $query->result_array();;

		return $array;
	}
}
?>