<?php

//This class is used to represent the schema of an Item in PharmaApp.
Class ItemSchema extends CI_Model
{
    //items table data
    private $item_id;
    private $item_name;
    private $item_description;
    private $brand;
    private $qty;

    //item_stock_dates table data
    private $date_purchased;
    private $est_restock_date;

    //The file, converted to an associative array.
    private $raw_input;

    //This will be a counter over the number of rows, so we know how many items will be in our for loop.
    private $numRows;


	public function __construct($raw)
	{
			parent::__construct();
			
            //Assign the raw input to an object variable.
            $raw_input = file($raw, FILE_SKIP_EMPTY_LINES);

            $numRows = count($raw_input);

            $item_id = $item_name = $item_description = $brand = $qty = $date_purchased = $est_restock_date = array();


	}

    private function populate_vars()
    {
        for($i = 0; $i < $numRows; $i++)
        {
            $item_id.push($raw_input[$i]['item_id']);
            $item_name.push($raw_input[$i]['item_name']);
            $item_description.push($raw_input[$i]['item_description']);
            $brand.push($raw_input[$i]['brand']);
            $qty.push($raw_input[$i]['qty']);
            $date_purchased.push($raw_input[$i]['date_purchased']);
            $est_restock_date.push($raw_input[$i]['est_restock_date']);
        }
    }

    public get_items()
    {
        // $item_id, $item_name, $item_description, $brand, $qty
    }

    public get_item_stock_dates()
    {
        // $item_id, $date_purchased, $est_restock_date   
    }

}







?>